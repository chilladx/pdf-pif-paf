Usage
=====

/!\ Before running, you **must** be in an active virtualenv. See [INSTALL.md](INSTALL.md) /!\

The script is self documented, with `--help` or `-h`.

```bash
(venv) $ ./pdf-pif-paf.py -h
usage: pdf-pif-paf.py [OPTIONS]

Script to merge PDF files (odd/even from a scanner) into a PDF file.

optional arguments:
  -h, --help       show this help message and exit
  --even EVEN      Path of the document with even pages (2, 4, 6, ...)
  --odd ODD        Path of the document with odd pages (1, 3, ...)
  --output OUTPUT  Path of the merged document
  --force          Force output file to be overridden
  --verbose        Verbose output
```

If you want to test the script against the fixtures files, try:

```bash
(venv) $ ./pdf-pif-paf.py \
 --even fixtures/14.pdf \
 --odd fixtures/15.pdf \
 --output fixtures/test.pdf \
 --force --verbose

(venv) $ ./pdf-pif-paf.py \
 --even fixtures/16.pdf \
 --odd fixtures/15.pdf \
 --output fixtures/test.pdf \
 --force --verbose

(venv) $ ./pdf-pif-paf.py \
 --even fixtures/4.pdf \
 --odd fixtures/15.pdf \
 --output fixtures/test.pdf \
 --force --verbose

(venv) $ ./pdf-pif-paf.py \
 --even fixtures/4.pdf \
 --odd fixtures/15.pdf \
 --force --verbose

(venv) $ ./pdf-pif-paf.py \
 --even fixtures/4.pdf \
 --odd fixtures/15.pdf \
 --output fixtures/test.pdf \
 --verbose

(venv) $ ./pdf-pif-paf.py \
 --even fixtures/4.pdf \
 --odd README.md \
 --output fixtures/test.pdf \
 --force --verbose
```
