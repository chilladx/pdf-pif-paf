Installation guide
==================

To install pdf-pif-paf, you only need to setup a virtualenv, install the dependencies in it, and run the script from it.

Setup virtualenv
----------------

```bash
$ cd /path/to/pdf-pif-paf
$ virtualenv venv -p /path/to/your/python3
```

Install dependencies
--------------------

```bash
$ source venv/bin/activate
(venv) $ pip install -r requirements.txt
```
