pdf-pif-paf
===========

What is it?
-----------

This python script allow you to merge to pdf documents, one with even pages, one with odd pages.

Documentation
-------------

The documentation available is included in markdown format in the docs/
directory.

Installation
------------

Read [docs/INSTALL.md](docs/INSTALL.md).

Licensing
---------

Read [LICENSE](LICENSE).
