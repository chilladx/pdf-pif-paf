#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import sys
import os.path
import logging
from PyPDF2 import PdfFileMerger, PdfFileReader

logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter(
        '%(asctime)s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)

if __name__ == '__main__':
    usage = 'pdf-pif-paf.py [OPTIONS]'
    description = 'Script to merge PDF files (odd/even from a scanner) into a PDF file.'

    argparser = argparse.ArgumentParser(description=description, usage=usage)
    argparser.add_argument("--even", help="Path of the document with even pages (2, 4, 6, ...)", type=str, required=True)
    argparser.add_argument("--odd", help="Path of the document with odd pages (1, 3, ...)", type=str, required=True)
    argparser.add_argument("--revert", help="Revert document with even pages (..., 6, 4, 2)", action='store_true', default=False)
    argparser.add_argument("--output", help="Path of the merged document", type=str, required=True)
    argparser.add_argument("--force", help="Force output file to be overridden", action='store_true', default=False)
    argparser.add_argument("--verbose", help="Verbose output", action='store_true', default=False)
    args = argparser.parse_args()

    if args.verbose:
        logger.setLevel(logging.DEBUG)

    logger.debug('Arguments:')
    logger.debug('\teven: %s', args.even)
    logger.debug('\todd: %s', args.odd)
    logger.debug('\trevert: %s', args.revert)
    logger.debug('\toutput: %s', args.output)
    logger.debug('\tforce: %s', args.force)
    logger.debug('\tverbose: %s', args.verbose)

    ## Opening files and PDF
    merger = PdfFileMerger()

    try:
        input1 = open(args.odd, "rb")
        input2 = open(args.even, "rb")
    except IOError as e:
        logger.error('Unable to open input file %s: %s', e.filename, e.strerror)
        sys.exit('Missing input file(s)')

    if os.path.isfile(args.output) and not args.force:
        logger.error('Output file already exists, not overriding %s', args.output)
        sys.exit('Output file already exists')

    try:
        output = open(args.output, "wb")
    except IOError as e:
        logger.error('Unable to open output file %s: %s', e.filename, e.strerror)
        sys.exit('Missing output file(s)')

    try:
        pdf1 = PdfFileReader(input1)
        pdf2 = PdfFileReader(input2)
        taille1 = pdf1.getNumPages()
        taille2 = pdf2.getNumPages()
    except Exception as e:
        logger.error('Unable to read input file as PDF: %s', e.args)
        sys.exit('Bad input file(s)')

    logger.debug('Doc sizes:')
    logger.debug('\tfile1: %s', taille1)
    logger.debug('\tfile2: %s', taille2)
    
    if (abs(taille1-taille2) > 1) or (taille2>taille1):
        logger.error('Huston, we have a problem: file size does not match. taille1: %s ; taille2: %s', taille1, taille2)
        sys.exit('Incompatible filesizes, exiting')

    # Loop is based on the smallest file
    mintaille = taille1 if taille1 < taille2 else taille2

    logger.debug('Starting to merge %s and %s together (revert is %s)', args.odd, args.even, args.revert)
    for i in range(1,mintaille+1):
        # add the i-th page of input files to output
        page=mintaille+1-i if args.revert else i
        logger.debug('   merging page %s for odd and %s for the even', i, page)
        merger.append(fileobj = input1, pages = (i-1,i))
        merger.append(fileobj = input2, pages = (page-1,page))

    # add the last page (if documents don't have the same page #)
    if taille1 > taille2:
        merger.append(fileobj = input1, pages = (taille1-1,taille1))
        logger.debug('   merged last page from input1: %s', taille1)

    # Write to an output PDF document
    logger.debug('Saving merged document in %s', args.output)
    merger.write(output)
    sys.exit(0)
