# Contribute to this repository

## Branch structure

We are using a [git flow](http://nvie.com/posts/a-successful-git-branching-model/) model for development.


We recommend that you create local working branches that target a specific scope of change. 


Each branch should be limited to a single feature/bugfix both to streamline workflows and reduce the possibility of merge conflicts.
![git flow picture](http://nvie.com/img/git-model@2x.png)